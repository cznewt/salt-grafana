from unittest.mock import MagicMock
from unittest.mock import patch

import pytest
import salt.utils.event
import salt.utils.json
import saltext.vector.engines.vector as vector_engine


@pytest.fixture
def configure_loader_modules():
    module_globals = {
        "__salt__": {"this_does_not_exist.please_replace_it": lambda: True},
    }
    return {
        vector_engine: module_globals,
    }


def test_replace_this_this_with_something_meaningful():
    assert "this_does_not_exist.please_replace_it" in vector_engine.__salt__
    assert vector_engine.__salt__["this_does_not_exist.please_replace_it"]() is True


def test_tag_matcher():
    assert vector_engine._match_tag("mytag", [], []) is True
    assert vector_engine._match_tag("mytag", ["my"], []) is False
    assert vector_engine._match_tag("mytag", ["mytag"], []) is True
    assert vector_engine._match_tag("mytag", [], ["mytag"]) is False
    assert vector_engine._match_tag("mytag", ["mytag"], ["mytag"]) is False
    assert vector_engine._match_tag("mytag", ["my*"], []) is True
    assert vector_engine._match_tag("mytag", [], ["*yta*"]) is False
    assert vector_engine._match_tag("mytag", ["my*"], ["mytag"]) is False
    assert vector_engine._match_tag("mytag", ["blah", "mytag"], []) is True
    assert vector_engine._match_tag("mytag", ["blah", "mytag"], []) is True
    assert vector_engine._match_tag("mytag", [], ["blah", "mytag"]) is False
    assert vector_engine._match_tag("minion/refresh/m1", ["minion/refresh/*"], []) is True
    assert vector_engine._match_tag("minion/refresh/m1", [], ["minion/refresh/*"]) is False
    assert vector_engine._match_tag("123", [], ["[0-9][0-9][0-9]"]) is False


def test_event_bus_context():
    ctx = vector_engine._event_bus_context({"__role": "master", "sock_dir": "/var/run/salt/master"})
    assert isinstance(ctx, salt.utils.event.MasterEvent)

    ctx = vector_engine._event_bus_context({"__role": "minion", "sock_dir": "/var/run/salt/minion"})
    assert isinstance(ctx, salt.utils.event.SaltEvent)


def test_config_validation():
    with patch.dict(vector_engine.__opts__, {"__role": "master", "id": "masterid"}):
        with pytest.raises(TypeError) as excinfo:
            vector_engine.start("127.0.0.1:9000", include_tags=123)
        assert "not a list" in str(excinfo.value)

        with pytest.raises(TypeError) as excinfo:
            vector_engine.start("127.0.0.1:9000", exclude_tags=123)
        assert "not a list" in str(excinfo.value)


def test_engine():
    TEST_DATA = [
        # opts, kwargs, event, event_assert
        [
            {"__role": "master", "id": "masterid", "sock_dir": "/tmp"},
            {},
            {"tag": "blah"},
            {"tag": "blah", "master_id": "masterid"},
        ],
        [
            {"__role": "minion", "id": "minionid", "sock_dir": "/tmp"},
            {},
            {"tag": "blah"},
            {"tag": "blah", "minion_id": "minionid"},
        ],
        [
            {"__role": "master", "id": "masterid", "sock_dir": "/tmp"},
            {"host_id": "mymaster"},
            {"tag": "blah"},
            {"tag": "blah", "master_id": "mymaster"},
        ],
        [
            {"__role": "master", "id": "masterid", "sock_dir": "/tmp"},
            {"exclude_tags": ["blah"]},
            {"tag": "blah"},
            {"tag": "blah"},
        ],
    ]
    for data in TEST_DATA:
        with patch.dict(vector_engine.__opts__, data[0]):
            socket = MagicMock()
            bus_context = MagicMock()
            event = data[2].copy()
            bus_context.return_value.__enter__.return_value.get_event.side_effect = [
                event,
                StopIteration(),
            ]
            with patch.object(vector_engine, "_event_bus_context", bus_context):
                with patch.object(vector_engine, "_connect", lambda addr: socket):
                    with pytest.raises(StopIteration) as excinfo:
                        vector_engine.start("127.0.1.1:9000", **data[1])
            assert event == data[3]
            if event == data[2]:
                socket.sendall.assert_not_called()
            else:
                socket.sendall.assert_called_once_with(
                    (salt.utils.json.dumps(data[3]) + "\n").encode("utf8")
                )
