# Salt Vector

[Salt](https://saltproject.io) engine to send events to [Vector](https://vector.dev)

This is a part of [Salt Grafana](https://turtletraction-oss.gitlab.io/salt-grafana/) project.
