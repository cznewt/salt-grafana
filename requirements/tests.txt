# SHA1:a69e233ca79420e13f017b1c8ff02a330fead481
#
# This file is autogenerated by pip-compile-multi
# To update, run:
#
#    pip-compile-multi
#
argcomplete==1.12.3
    # via nox
attrs==22.1.0
    # via
    #   pytest
    #   pytest-salt-factories
    #   pytest-skip-markers
click==8.1.3
    # via flask
colorlog==6.7.0
    # via nox
coverage[toml]==6.2
    # via
    #   -r requirements/tests.in
    #   pytest-cov
distlib==0.3.6
    # via virtualenv
distro==1.8.0
    # via pytest-skip-markers
exceptiongroup==1.0.4 ; python_version >= "3.7"
    # via -r requirements/tests.in
filelock==3.4.1
    # via
    #   -r requirements/tests.in
    #   virtualenv
flask==2.2.2
    # via pytest-flask
importlib-metadata==5.0.0
    # via flask
iniconfig==1.1.1
    # via pytest
itsdangerous==2.1.2
    # via flask
jinja2==3.1.2
    # via flask
markupsafe==2.1.1
    # via
    #   jinja2
    #   werkzeug
msgpack==1.0.4
    # via pytest-salt-factories
nox==2022.1.7
    # via -r requirements/tests.in
packaging==21.3
    # via
    #   nox
    #   pytest
platformdirs==2.4.0
    # via
    #   -r requirements/tests.in
    #   virtualenv
pluggy==1.0.0
    # via pytest
psutil==5.9.4
    # via pytest-salt-factories
py==1.11.0
    # via
    #   nox
    #   pytest
pyparsing==3.0.9
    # via packaging
pytest==7.0.1
    # via
    #   -r requirements/tests.in
    #   pytest-cov
    #   pytest-flask
    #   pytest-helpers-namespace
    #   pytest-salt-factories
    #   pytest-skip-markers
    #   pytest-tempdir
pytest-cov==4.0.0
    # via -r requirements/tests.in
pytest-flask==1.2.0
    # via -r requirements/tests.in
pytest-helpers-namespace==2021.12.29
    # via pytest-salt-factories
pytest-salt-factories==0.912.2
    # via -r requirements/tests.in
pytest-skip-markers==1.3.0
    # via pytest-salt-factories
pytest-tempdir==2019.10.12
    # via pytest-salt-factories
pyzmq==24.0.1
    # via pytest-salt-factories
tomli==1.2.3
    # via
    #   -r requirements/tests.in
    #   coverage
    #   pytest
virtualenv==20.16.7
    # via
    #   nox
    #   pytest-salt-factories
werkzeug==2.2.2
    # via
    #   flask
    #   pytest-flask
zipp==3.10.0
    # via importlib-metadata
