# Tempo Relay

Web service to consume events from [Vector](https://vector.dev) and send them to [Grafana Tempo](https://grafana.com/docs/tempo/latest/).

This is a part of [Salt Grafana](https://turtletraction-oss.gitlab.io/salt-grafana/) project.
