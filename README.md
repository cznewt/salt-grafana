# Salt observability tool based on Grafana

![Overview dashboard](docs/_static/dashboard-overview.png)

![Job view dashboard](docs/_static/dashboard-job-view.png)

## Documentation

Documentation is available at [turtletraction-oss.gitlab.io/salt-grafana](https://turtletraction-oss.gitlab.io/salt-grafana/).

This project was made possible by:

* Erick Alphonse and Nils Martin ([idaaas.com](http://idaaas.com))
* Max Arnold ([salt.tips](https://salt.tips) and [turtletraction.com](https://turtletraction.com))
* Kristoffer Granberg Cauchi ([turtletraction.com](https://turtletraction.com))

See the [AUTHORS](AUTHORS) file for a full list of contributors.
